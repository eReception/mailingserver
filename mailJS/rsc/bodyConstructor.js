module.exports.getNotfalllistBody = function (besucherListe) {
	
	var liste = "<!DOCTYPE html>" +
				'<html>' +
	
				'<body>' +
	
				
				'<p>Sehr geehrte Damen und Herren,</p>' +
				'<p>Es gibt ein Notfall. Folgende Personen befinden sich aktuell noch im Gebäude:</p>' +
				
				'<h2 style="color: #2e6c80;">Notfallliste:</h2>' +
				'<ol style="list-style: none; font-size: 14px; line-height: 32px; font-weight: bold;">';
				
			
			var l = besucherListe.length;
			var  i;
			for (i = 0; i < l; i++){
				liste = liste + '<li style="clear: both;">' + besucherListe[i].vorname + ' ' + besucherListe[i].nachname + '. Von der Firma ' + besucherListe[i].firma + '.</li>'; 
			}

				liste = liste + '</ol>' +
				'<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</p>' +
				
	  
				'<p><strong>&nbsp;</strong></p>' +
				'<p><strong>Mit freundlichen Grüßen</strong></p>' +
				'<p><strong>Ihr eReception Team</strong></p>' +
				'<p><img src="https://www.acbanet.org/wp-content/uploads/2014/07/summer-reception-2014-glasses-banner.jpg" alt="interactive connection" width="830" height="120" /></p>' +
				'</body>' +
				'</html>';
				
				return liste;
};


module.exports.getOnlineCheckInBody = function (anrede, nachname) {
	
	return "<!DOCTYPE html>" +
				'<html>' +
	
				'<body>' +
	
				'<h1 style="color: #5e9ca0;">eReception</h1>' +
				'<h2 style="color: #2e6c80;">Check-In Best&auml;tigung:</h2>' +
				'<p>Hallo ' + anrede + ' ' + nachname + ',</p>' +
				'<p>Vielen Dank für die Anmeldung. Mit dem Unteren Barcode können Sie bequem und schnell am tag ires besuches einchecken.</p>' +
				
	
				'<center><p><img src="https://images-eu.ssl-images-amazon.com/images/I/31Umxl57vfL.png" alt="interactive connection" width="300" height="300" /></p></center>' +
	
				/*'<h2 style="color: #2e6c80;">Liste von Regeln und Voraussetzungen:</h2>' +
				'<ol style="list-style: none; font-size: 14px; line-height: 32px; font-weight: bold;">' +
				'<li style="clear: both;">Punkt 1</li>' +
				'<li style="clear: both;">Punkt 2</li>' +
				'<li style="clear: both;">Punkt 3</li>' +
				'</ol>' +
				'<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</p>' +
				'<h2 style="color: #2e6c80;">Regeln:</h2>' +*/ 
	  
				'<p><strong>&nbsp;</strong></p>' +
				'<p><strong>Viel Spa&szlig;</strong></p>' +
				'<p><strong>Ihr eReception Team</strong></p>' +
				'<p><img src="https://www.acbanet.org/wp-content/uploads/2014/07/summer-reception-2014-glasses-banner.jpg" alt="interactive connection" width="830" height="120" /></p>' +
				'</body>' +
				'</html>';
};

module.exports.getMitarbeiterBenachrichtigungBody = function (besucherAnrede, besucherNachname) {
	
	return "<!DOCTYPE html>" +
				'<html>' +
	
				'<body>' +
	
				'<h1 style="color: #5e9ca0;">eReception</h1>' +
				'<p>Sie haben einen Besuch von ' + besucherAnrede + ' ' + besucherNachname + ' bekommen. Bitte kommen Sie zum Empfang, um ihn/sie abzuholen.</p>' +
	  
				'<p><strong>&nbsp;</strong></p>' +
				'<p><strong>Viel Grüße.</strong></p>' +
				'<p><strong>Ihr eReception Team</strong></p>' +
				'<p><img src="https://www.acbanet.org/wp-content/uploads/2014/07/summer-reception-2014-glasses-banner.jpg" alt="interactive connection" width="830" height="120" /></p>' +
				'</body>' +
				'</html>';
};


