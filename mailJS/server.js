	/*eslint no-console: 0*/
	"use strict";
	const express = require("express");
	var nodemailer = require("nodemailer");
	var bodyConst = require("./rsc/bodyConstructor.js");

	const app = express();
	var transporter, mailOptions;

	app.use(express.json());

	app.get("/health", function (req, res) {
		res.statusCode = 200;
		res.statusMessage = "OK";
		res.send("Running ...");

	});
	/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
	app.post("/sendeNotfallliste", function (req, res) {

		transporter = nodemailer.createTransport({
			service: "gmail",
			auth: {
				user: "no.reply.ereception@gmail.com",
				pass: "FST2020ereception"
			}
		});
		var notfallBody = bodyConst.getNotfalllistBody(req.body);
		mailOptions = {
			from: "no.reply.ereception@gmail.com",
			to: "kadriye_kyubi@web.de,doboe002@fh-dortmund.de",
			subject: "eReception: Notfall !",
			html: notfallBody
		};

		transporter.sendMail(mailOptions, function (error, info) {
			if (error) {
				console.log(error);
			} else {
				console.log('Email sent: ' + info.response);
			}
		});
		res.writeHead(200, {
			"Content-Type": "text/plain"
		});

		res.statusCode = 200;
		res.statusMessage = 'ok';

		res.end();

		//res.send(req.body);
	});
	/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
	app.post("/check_In_bestaitigung", function (req, res) {

		transporter = nodemailer.createTransport({
			service: "gmail",
			auth: {
				user: "no.reply.ereception@gmail.com",
				pass: "FST2020ereception"
			}
		});

		mailOptions = {
			from: "no.reply.ereception@gmail.com",
			to: req.body.empfaenger,
			subject: "eReception: Check-In Bestätigung",
			html: bodyConst.getOnlineCheckInBody(req.body.besucherAnrede, req.body.besucherNachname)
		};

		transporter.sendMail(mailOptions, function (error, info) {
			if (error) {
				console.log(error);
			} else {
				console.log('Email sent: ' + info.response);
			}
		});
		res.writeHead(200, {
			"Content-Type": "text/plain"
		});

		res.statusCode = 200;
		res.statusMessage = 'ok';

		res.end();

		//res.send(req.body);
	});
	
	app.post("/mitarbeiter_benachrichtigung", function (req, res) {

		transporter = nodemailer.createTransport({
			service: "gmail",
			auth: {
				user: "no.reply.ereception@gmail.com",
				pass: "FST2020ereception"
			}
		});

		mailOptions = {
			from: "no.reply.ereception@gmail.com",
			to: req.body.empfaenger,
			subject: "eReception: Sie haben Besuch",
			html: bodyConst.getMitarbeiterBenachrichtigungBody(req.body.besucherAnrede, req.body.besucherNachname )
		};

		transporter.sendMail(mailOptions, function (error, info) {
			if (error) {
				console.log(error);
			} else {
				console.log('Email sent: ' + info.response);
			}
		});
		res.writeHead(200, {
			"Content-Type": "text/plain"
		});

		res.statusCode = 200;
		res.statusMessage = 'ok';

		res.end();

		//res.send(req.body);
	});
	
	const port = process.env.PORT || 3000;
	app.listen(port, function () {
		console.log("Server listening on port %d", port);
	});