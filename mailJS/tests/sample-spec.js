
// use test spec file name as description to allow navigation from the test results view
const bodyConst = require('../rsc/bodyConstructor.js');


test('Firstname and lastname were inserted in Mail body', () => {
	//expect(bodyConst.sum(1, 2)).toBe(3);
  expect(bodyConst.getBody('Christoph', 'Colombo')).toMatch(/Christoph/);
  //expect(bodyConst.getBody('Christoph', 'Colombo')).toMatch(/Colombo/);
});

/*describe(__filename, function () {

    it("First test", function(done) {
        expect(false).toBe(false);
        done();
    });
});*/